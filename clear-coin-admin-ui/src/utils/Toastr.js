import toastr from "toastr";

toastr.options = {
  closeButton: true,
  debug: false,
  newestOnTop: false,
  progressBar: true,
  positionClass: "toast-top-right",
  preventDuplicates: true,
  onclick: null,
  showDuration: "300",
  hideDuration: "1000",
  timeOut: "10000",
  extendedTimeOut: "1000",
  showEasing: "swing",
  hideEasing: "linear",
  showMethod: "fadeIn",
  hideMethod: "fadeOut",
};

class Toastr {
  static error(message) {
    toastr.error(message);
  }

  static success(message) {
    toastr.success(message);
  }

  static warning(message) {
    toastr.warning(message);
  }

  static info(message) {
    toastr.info(message);
  }
}

export default Toastr;
