import { Link } from "react-router-dom";

const Nav = () => {
  return (
    <nav>
      <Link to="/" className="brand">
        <i className="fab fa-contao"></i>
      </Link>
      <Link to="/users" className="brand">
        Users
      </Link>
      <Link to="/operations" className="brand">
        Operations
      </Link>
    </nav>
  );
};

export default Nav;
