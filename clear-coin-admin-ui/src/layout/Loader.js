import React, { Fragment } from "react";

const Loader = ({ loading, content, children }) => {
  const loadingContent = (
    <div className="dimmer active m-5">
      <div className="loader"></div>
      <div className="dimmer-content">
        {content ? (
          content
        ) : (
          <p>
          </p>
        )}
      </div>
    </div>
  );

  return <Fragment>{loading === true ? loadingContent : children}</Fragment>;
};

export default Loader;
