import logo from "./logo.svg";
import "./App.css";
import HomeHeader from "./layout/HomeHeader";
import Nav from "./layout/Nav";
import Footer from "./layout/Footer";
import { Route, Switch } from "react-router-dom";
import Four04 from "./core/Four04";
import Home from "./pages/Home";
import Users from "./pages/Users";
import Operations from "./pages/Operations";

function App() {
  return (
    <div className="App">
      <Nav />
      <Route exact path="/" component={HomeHeader} />

      <main className="content">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/users" component={Users} />
          <Route path="/operations" component={Operations} />
          <Route component={Four04} />
        </Switch>
      </main>

      <Footer />
    </div>
  );
}

export default App;
