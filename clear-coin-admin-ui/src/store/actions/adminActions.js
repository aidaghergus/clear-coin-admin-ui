import * as actions from "../actionTypes";

export function getUsersSuccess(products) {
  return {
    type: actions.GET_USERS_SUCCESS,
    payload: products,
  };
}
export function getUsersError(error) {
  return {
    type: actions.GET_USERS_ERROR,
    payload: error,
  };
}
export function setLoading() {
  return {
    type: actions.SET_LOADING,
  };
}

export function getUsers() {
  return function (dispatch) {
    setLoading();
    return fetch(`http://localhost:8080/users`)
      .then((rep) => {
        if (!rep.ok) throw Error(rep);
        return rep.json();
      })
      .then((data) => dispatch(getUsersSuccess(data)))
      .catch((err) => dispatch(getUsersError(err.message)));
  };
}
