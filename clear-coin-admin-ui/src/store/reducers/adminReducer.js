import * as actions from "../actionTypes";

const initialState = {
  users: [],
  usersErrorMessage: "",
  loading: false,
};

const adminReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.GET_USERS_SUCCESS:
      return {
        ...state,
        users: [...action.payload],
        usersErrorMessage: "",
        loading: false,
      };
    case actions.GET_USERS_ERROR:
      return {
        ...state,
        users: [],
        usersErrorMessage: action.payload,
        loading: false,
      };
    case actions.SET_LOADING:
      return {
        ...state,
        users: [],
        usersErrorMessage: action.payload,
        loading: true,
      };
    default:
      return state;
  }
};

export default adminReducer;
