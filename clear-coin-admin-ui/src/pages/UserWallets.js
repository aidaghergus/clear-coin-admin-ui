import React, { useEffect, useState } from "react";
import Modal from "react-modal";
import Wallets from "./Wallets";

const UserWallets = ({ userWalletsOpen, closeUserWalletsModal, user }) => {
  const [wallets, setWallets] = useState([]);
  const [error, setError] = useState("");

  useEffect(() => {
    fetch(`http://localhost:8080/users/wallets?user=${user.id}`)
      .then((rep) => {
        if (!rep.ok) throw Error(rep);
        return rep.json();
      })
      .then((data) => setWallets(data))
      .catch((err) => setError(err));
  }, []);

  return (
    <Modal
      style={{
        content: {
          top: "100px",
          left: "200px",
          right: "200px",
          bottom: "140px",
        },
      }}
      isOpen={userWalletsOpen}
      onRequestClose={closeUserWalletsModal}
    >
      <div className="card" style={{ height: "100%" }}>
        <div className="card-body">
          <h4 className="card-title">{user.name}</h4>
          <h6 className="card-subtitle mb-2 text-muted">{user.email}</h6>
          <p className="card-text">{error}</p>
          <Wallets wallets={wallets} />
        </div>
        <div className="card-footer">
          <button
            onClick={closeUserWalletsModal}
            className="btn btn-info btn-sm"
          >
            {" "}
            Close{" "}
          </button>
        </div>
      </div>
    </Modal>
  );
};

Modal.setAppElement("#root");

export default UserWallets;
