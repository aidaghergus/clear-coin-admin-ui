import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { getUsers } from "../store/actions/adminActions";
import UserWallets from "./UserWallets";

const Users = (props) => {
  const [openedUser, setOpenedUser] = useState(null);
  const [isOpenedUser, setIsOpenedUser] = useState(false);

  const openUser = (user) => {
    setOpenedUser(user);
  };

  const closeUser = () => {
    setOpenedUser(null);
    setIsOpenedUser(false);
  };

  useEffect(() => {
    props.getUsers();
  }, []);

  useEffect(() => {
    openedUser !== null && setIsOpenedUser(true);
  }, [openedUser]);
  return (
    <>
      {!props.users ||
        (props.users.length === 0 && <span>No users found</span>)}
      <ul className="content list-group user-area">
        {props.users.map((user) => (
          <li key={user.id} className="list-group-item">
            <div className="row">
              <div className="col-4">{user.name}</div>
              <div className="col-4">{user.email}</div>
              <div className="col-2">
                {user.wallets ? user.wallets.length : 0} wallets
              </div>
              <div className="col-2">
                <button
                  type="button"
                  className="btn btn-info"
                  onClick={() => openUser(user)}
                >
                  View
                </button>
              </div>
            </div>
          </li>
        ))}
      </ul>
      )
      {isOpenedUser && (
        <UserWallets
          userWalletsOpen={isOpenedUser}
          closeUserWalletsModal={closeUser}
          user={openedUser}
        />
      )}
    </>
  );
};

const mapStateToProps = (state, ownProps) => {
  return {
    users: state.admin.users,
    usersError: state.admin.errorMessage,
    loading: state.admin.loading,
  };
};

export default connect(mapStateToProps, { getUsers })(Users);
