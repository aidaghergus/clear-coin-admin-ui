import React from "react";

const Wallets = ({ wallets }) => {
  return (
    <>
      {!wallets || (wallets.length === 0 && <span>No wallets found.</span>)}
      <table className="table table-light">
        <thead className="thead-light">
          <tr>
            <th scope="col">Wallet Address</th>
            <th scope="col">Balance</th>
          </tr>
        </thead>
        <tbody>
          {wallets.map((wallet) => (
            <tr key={wallet.id}>
              <td>{wallet.id}</td>
              <td>{wallet.balance}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
};

export default Wallets;
