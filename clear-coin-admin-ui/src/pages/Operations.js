import React, { useEffect, useState } from "react";
import Wallets from "./Wallets";
import ReactLoading from "react-loading";
import Toastr from "../utils/Toastr";

const Operations = (props) => {
  const [wallets, setWallets] = useState([]);
  const [error, setError] = useState("");
  const [from, setFrom] = useState("");
  const [to, setTo] = useState("");
  const [amount, setAmount] = useState(0);
  const [loading, setLoading] = useState(true);

  const getWallets = () => {
    fetch(`http://localhost:8080/wallets`)
      .then((rep) => {
        if (!rep.ok) throw Error(rep);
        return rep.json();
      })
      .then((data) => setWallets(data))
      .catch((err) => setError(err));
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    fetch("http://localhost:8080/wallets/transfer", {
      method: "post",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({ from: from, to: to, amount: amount }),
    })
      .then((response) => {
        console.log(response);
        if (!response.ok) {
          Toastr.error("Not enough coins");
          console.log("a");
        } else {
          Toastr.info("Transfer done");
          setLoading(true);
          getWallets();
        }
      })
      .catch((err) => Toastr.error("Something went wrong"));
  };
  const handleFromChange = (event) => {
    setFrom(event.target.value);
  };
  const handleToChange = (event) => {
    setTo(event.target.value);
  };
  const handleAmountChange = (event) => {
    setAmount(event.target.value);
  };
  useEffect(() => {
    setLoading(false);
  }, [wallets, error]);

  useEffect(() => {
    getWallets();
  }, []);
  return (
    <div className="operations-area">
      <form onSubmit={handleSubmit}>
        <label style={{ width: "30%" }}>
          From:
          <input type="text" value={from} onChange={handleFromChange} />
        </label>
        <label style={{ width: "30%" }}>
          To:
          <input type="text" value={to} onChange={handleToChange} />
        </label>
        <label style={{ width: "30%" }}>
          Amount:
          <input type="number" value={amount} onChange={handleAmountChange} />
        </label>
        <input
          style={{ width: "10%" }}
          className="btn-info"
          type="submit"
          value="Transfer"
        />
      </form>
      <div>{error}</div>
      {loading ? (
        <ReactLoading type={"spin"} color="rgb(80, 232, 255)" />
      ) : (
        <Wallets wallets={wallets} />
      )}
    </div>
  );
};

export default Operations;
